export const environment = {
  production: true,
  mapboxKey: 'pk.eyJ1IjoiYXJtYW5kby1yaXZlcmEiLCJhIjoiY2tidmc0eHh5MDVxZjMycWZpenByMWtmZiJ9.yOC42dTFZzhulcp7o3vyNA',
  URL_API: 'https://api.kairosshop.xyz/api'
};
